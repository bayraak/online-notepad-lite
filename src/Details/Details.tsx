import React, { Component } from 'react';
import { TextArea } from 'grommet';


class Details extends React.Component {


    render() {
        return (
            <React.Fragment>
                <div id="footer" className="footer">
                    <div>
                        About
                       <span>
                            <span id="modifiedFooter" className="hidden">*</span>
                            <span id="lblLegacyFS" className="hidden footer-label">Legacy Mode</span>
                            <span id="lblTabMovesFocus" className="hidden footer-label">Tab Moves Focus</span>
                        </span>
                    </div>
                    <div id="not-supported">
                        The
                      <a href="https://wicg.github.io/native-file-system/" target="_blank">Native File System API</a>
                        is <b>not</b> supported in this browser yet, and Text Editor is running
                        in legacy mode. In legacy mode, the file modified indicators are not shown.
                        If you're running Chrome, you can enable the Native File System
                         APIs by enabling the <code>#native-file-system-api</code>
                        flag in <code>chrome://flags</code>.
                  </div>
                    <div>
                        Text Editor is a simple text editor similar to notepad that makes
                        it easy to edit text files on your local file system. It shows how
                        to easily use the new HTML5 Native File System APIs. To learn more
                        about the Native File System APIs and how this demo was built, check out
                  <a href="https://developers.google.com/web/updates/2018/11/writable-files" target="_blank">
                            The native file system APIs</a> article on Web Fundamentals, or see the
                     <a href="https://github.com/GoogleChromeLabs/text-editor/" target="_blank">
                            source code on GitHub</a>.
                        Written by <a href="https://twitter.com/petele" target="_blank">Pete LePage</a>.

                    </div>
                </div>

            </React.Fragment>
        )
    }
}

export default Details