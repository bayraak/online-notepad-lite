import React, { Component, useEffect, useState } from 'react';
import { TextArea } from 'grommet';


const Editor = () => {

    const [isModified, setModified] = useState(false);
    const [value, setValue] = useState('');

    useEffect(() => {
        // textArea.style.fontSize = `${app.options.fontSize}px`;
        // app.setFocus();
    }, [])

    const handleChange = (e: any) => {
        // app.setModified(true);
        if (!isModified)
            setModified(true);

        setValue(e?.target?.value)
    }

    const handleKeydown = (e: any) => {
        // if (e.key === 'Tab' && app.options.captureTabs) {
        if (e.key === 'Tab') {
            let val = value;
            setValue(val += '\t');
            e.preventDefault();
            // app.insertIntoDoc('\t');
        }
    }

    const handleFocus = (e: any) => {
        // myMenus.hideAll();
        // console.log('focusin')
    }


    return (
        <React.Fragment>
            <TextArea
                style={{
                    height: '100%',
                    border: 'none',
                    fontFamily: `'Anonnymous Pro', monospace`,
                    fontWeight: 400,
                    fontSize: '1.5rem'
                }}
                id="textEditor"
                autoFocus
                spellCheck={true}
                onChange={handleChange}
                onKeyDownCapture={handleKeydown}
                onFocus={handleFocus}
                value={value}
            />
            {/* <input type="file" id="filePicker" /> */}
        </React.Fragment>
    )
}

export default Editor