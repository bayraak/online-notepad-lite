import React, { Component } from 'react';
import {
  Box,
  Button,
  Collapsible,
  Heading,
  Grommet,
  Layer,
  ResponsiveContext,
  Footer,
  Anchor,
  Text
} from 'grommet';
import { FormClose } from 'grommet-icons';
import Editor from './Editor/Editor';
import PrivacyPolicy from './PrivacyPolicy/PrivacyPolicy';
import './App.css'
import TermsConditions from './TermsConditions/TermsConditions';

const theme = {
  global: {
    colors: {
      brand: '#292961',
    },
    font: {
      family: 'Roboto',
      size: '14px',
      height: '20px',
    },
  },
};

const AppBar = (props: any) => (
  <Box
    tag='header'
    direction='row'
    align='center'
    justify='between'
    background='brand'
    pad={{ left: 'medium', right: 'small', vertical: 'small' }}
    elevation='medium'
    style={{ zIndex: '1' }}
    {...props}
  />
);

function initEventListeners() {
  window.addEventListener('keydown', (e) => {
    // console.log('key', e.code, e.ctrlKey, e.metaKey, e.shiftKey, e.key);

    // Save As
    if ((e.ctrlKey || e.metaKey) && e.shiftKey && e.code === 'KeyS') {
      e.preventDefault();
      // app.saveFileAs();
      console.log('SaveFileAs')
      return;
    }

    // Save
    if ((e.ctrlKey === true || e.metaKey === true) && e.key === 's') {
      e.preventDefault();
      // app.saveFile();
      console.log('SAveFile')
      return;
    }

    // Open
    if ((e.ctrlKey === true || e.metaKey === true) && e.key === 'o') {
      e.preventDefault();
      // app.openFile();
      console.log('openFile')
      return;
    }

    // Close
    if ((e.ctrlKey === true || e.metaKey === true) && e.key === 'n') {
      e.preventDefault();
      // app.newFile();
      console.log('newFile')
      return;
    }

    // Quit
    if ((e.ctrlKey === true || e.metaKey === true) &&
      (e.key === 'q' || e.key === 'w')) {
      e.preventDefault();
      // app.quitApp();
      console.log('quitApp')
      return;
    }

    // Capture Tabs
    if (e.ctrlKey === true && e.shiftKey === true && e.key === 'M') {
      e.preventDefault();
      e.stopPropagation();
      // app.toggleCaptureTabs();
      console.log('Tabs')
    }
  });
}

class App extends Component {

  state = {
    showSidebar: false,
    showPrivacy: false,
    showTerms: false
  }

  // componentDidMount() {
  //   // initEventListeners();
  // }

  render() {
    const { showSidebar, showPrivacy, showTerms } = this.state;


    return (
      <Grommet theme={theme} full>

        <ResponsiveContext.Consumer>
          {(size: string) => (
            <React.Fragment>
              <Box fill>
                <AppBar>
                  <Heading level='1' size='xs' margin='none'>Online Notepad Lite</Heading>
                  <Heading level='4' size='xs' margin={{ left: '10' }}>Online text editor, online notepad, online text playground</Heading>
                  {/* <Button
                  icon={<Services />}
                  onClick={() => this.setState({ showSidebar: !this.state.showSidebar })}
                /> */}
                </AppBar>

                {showPrivacy && (
                  <Layer
                    onEsc={() => this.setState({ showPrivacy: false })}
                    onClickOutside={() => this.setState({ showPrivacy: false })}
                  >
                    <Box
                      direction="row"
                      border={{ color: 'brand', size: 'large' }}
                      pad="medium"
                      justify={'between'}
                    >
                      <Heading level='1' size='m' margin='none'>Privacy Policy</Heading>
                      <Button label="close" onClick={() => this.setState({ showPrivacy: false })} />
                    </Box>
                    <Box
                      direction="row"
                      pad="medium"
                    >
                      <PrivacyPolicy />
                    </Box>
                  </Layer>
                )}

                {showTerms && (
                  <Layer
                    onEsc={() => this.setState({ showTerms: false })}
                    onClickOutside={() => this.setState({ showTerms: false })}
                  >
                    <Box
                      direction="row"
                      border={{ color: 'brand', size: 'large' }}
                      pad="medium"
                      justify={'between'}
                    >
                      <Heading level='1' size='m' margin='none'>Terms and Conditions</Heading>
                      <Button label="close" onClick={() => this.setState({ showTerms: false })} />
                    </Box>
                    <Box
                      direction="row"
                      pad="medium"
                    >
                      <TermsConditions />
                    </Box>
                  </Layer>
                )}


                <Box direction='row' flex overflow={{ horizontal: 'hidden' }}>
                  <Box flex align='center' justify='center'>

                    <Editor></Editor>

                  </Box>
                  {(!showSidebar || size !== 'small') ? (
                    <Collapsible direction="horizontal" open={showSidebar}>
                      <Box
                        flex
                        width='medium'
                        background='light-2'
                        elevation='small'
                        align='center'
                        justify='center'
                      >
                        sidebar
                    </Box>
                    </Collapsible>
                  ) : (
                      <Layer>
                        <Box
                          background='light-2'
                          tag='header'
                          justify='end'
                          align='center'
                          direction='row'
                        >
                          <Button
                            icon={<FormClose />}
                            onClick={() => this.setState({ showSidebar: false })}
                          />
                        </Box>
                        <Box
                          fill
                          background='light-2'
                          align='center'
                          justify='center'
                        >
                          sidebar
                    </Box>
                      </Layer>
                    )}
                </Box>
              </Box>
              <Box>
                <Text textAlign='center' size='small'>By using our site, you acknowledge that you have read and understand our Privacy Policy, and our Terms of Service</Text>
              </Box>
              <Footer background="brand" pad="medium">
                <Button size='small' label='Privacy and Policy' onClick={() => this.setState({ showPrivacy: true })} />
                <Button size='small' label='Terms and Conditions' onClick={() => this.setState({ showTerms: true })} />
              </Footer>
            </React.Fragment>
          )}
        </ResponsiveContext.Consumer>
      </Grommet>
    );
  }
}

export default App;