import { Box, Button, Layer, Text, Heading, Paragraph } from "grommet";
import React from "react";

const PrivacyPolicy = () => {
    const [show, setShow] = React.useState(false);

    return (
        <Box height="medium" overflow="auto">
            <Heading level='3' size='m'>PRIVACY NOTICE</Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Last updated April 11, 2020
        </Paragraph>



            <Paragraph size='small' style={{maxWidth: 550}}>
                Thank you for choosing to be part of our community at OnlineNotepadLite (“Company”, “we”, “us”, or “our”). We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about our notice, or our practices with regards to your personal information, please contact us at bayraak@gmail.com.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                When you visit our website https://onlinenotepadlite.com, and use our services, you trust us with your personal information. We take your privacy very seriously. In this privacy notice, we seek to explain to you in the clearest way possible what information we collect, how we use it and what rights you have in relation to it. We hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy notice that you do not agree with, please discontinue use of our Sites and our services.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                This privacy notice applies to all information collected through our website (such as https://onlinenotepadlite.com), and/or any related services, sales, marketing or events (we refer to them collectively in this privacy notice as the "Services").
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Please read this privacy notice carefully as it will help you make informed decisions about sharing your personal information with us.
        </Paragraph>



            <Heading level='3' size='m'>
                TABLE OF CONTENTS
        </Heading>

            <ol>
                <li> WHAT INFORMATION DO WE COLLECT</li>

                <li> WILL YOUR INFORMATION BE SHARED WITH ANYONE</li>

                <li> WHAT IS OUR STANCE ON THIRD-PARTY WEBSITES</li>

                <li> HOW LONG DO WE KEEP YOUR INFORMATION</li>

                <li> HOW DO WE KEEP YOUR INFORMATION SAFE</li>

                <li> DO WE COLLECT INFORMATION FROM MINORS</li>

                <li> WHAT ARE YOUR PRIVACY RIGHTS</li>

                <li> CONTROLS FOR DO-NOT-TRACK FEATURE</li>

                <li> DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS</li>

                <li> DO WE MAKE UPDATES TO THIS POLICY</li>

                <li> HOW CAN YOU CONTACT US ABOUT THIS POLICY</li>
            </ol>

            <Heading level='3' size='m'>
                1. WHAT INFORMATION DO WE COLLECT?
        </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Information automatically collected
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short:   Some information — such as IP address and/or browser and device characteristics — is collected automatically when you visit our Services.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                We automatically collect certain information when you visit, use or navigate the Services. This information does not reveal your specific identity (like your name or contact information) but may include device and usage information, such as your IP address, browser and device characteristics, operating system, language preferences, referring URLs, device name, country, location, information about how and when you use our Services and other technical information. This information is primarily needed to maintain the security and operation of our Services, and for our internal analytics and reporting purposes.
        </Paragraph>


            <Heading level='3' size='m'>
                2. WILL YOUR INFORMATION BE SHARED WITH ANYONE?
        </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short:  We only share information with your consent, to comply with laws, to provide you with services, to protect your rights, or to fulfill business obligations.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                We may process or share data based on the following legal basis:
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Consent: We may process your data if you have given us specific consent to use your personal information in a specific purpose.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Legitimate Interests: We may process your data when it is reasonably necessary to achieve our legitimate business interests.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Performance of a Contract: Where we have entered into a contract with you, we may process your personal information to fulfill the terms of our contract.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Legal Obligations: We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Vital Interests: We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                More specifically, we may need to process your data or share your personal information in the following situations:
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Vendors, Consultants and Other Third-Party Service Providers. We may share your data with third party vendors, service providers, contractors or agents who perform services for us or on our behalf and require access to such information to do that work. Examples include: payment processing, data analysis, email delivery, hosting services, customer service and marketing efforts. We may allow selected third parties to use tracking technology on the Services, which will enable them to collect data about how you interact with the Services over time. This information may be used to, among other things, analyze and track data, determine the popularity of certain content and better understand online activity. Unless described in this Policy, we do not share, sell, rent or trade any of your information with third parties for their promotional purposes.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Business Transfers. We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.
        </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Third-Party Advertisers. We may use third-party advertising companies to serve ads when you visit the Services. These companies may use information about your visits to our Website(s) and other websites that are contained in web cookies and other tracking technologies in order to provide advertisements about goods and services of interest to you.
        </Paragraph>


            <Heading level='3' size='m'>
                3. WHAT IS OUR STANCE ON THIRD - PARTY WEBSITES ?
        </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short: We are not responsible for the safety of any information that you share with third - party providers who advertise, but are not affiliated with, our websites.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                The Services may contain advertisements from third parties that are not affiliated with us and which may link to other websites, online services or mobile applications.We cannot guarantee the safety and privacy of data you provide to any third parties.Any data collected by third parties is not covered by this privacy notice.We are not responsible for the content or privacy and security practices and policies of any third parties, including other websites, services or applications that may be linked to or from the Services.You should review the policies of such third parties and contact them directly to respond to your questions.
            </Paragraph>

            <Heading level='3' size='m'>
                4. HOW LONG DO WE KEEP YOUR INFORMATION ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short: We keep your information for as long as necessary to fulfill the purposes outlined in this privacy notice unless otherwise required by law.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy notice, unless a longer retention period is required or permitted by law(such as tax, accounting or other legal requirements).No purpose in this policy will require us keeping your personal information for longer than 90 days.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize it, or, if this is not possible(for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.
            </Paragraph>

            <Heading level='3' size='m'>
                5. HOW DO WE KEEP YOUR INFORMATION SAFE ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short: We aim to protect your personal information through a system of organizational and technical security measures.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                We have implemented appropriate technical and organizational security measures designed to protect the security of any personal information we process.However, please also remember that we cannot guarantee that the internet itself is 100 % secure.Although we will do our best to protect your personal information, transmission of personal information to and from our Services is at your own risk.You should only access the services within a secure environment.
            </Paragraph>

            <Heading level='3' size='m'>
                6. DO WE COLLECT INFORMATION FROM MINORS ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short: We do not knowingly collect data from or market to children under 18 years of age.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                We do not knowingly solicit data from or market to children under 18 years of age.By using the Services, you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to such minor dependent’s use of the Services.If we learn that personal information from users less than 18 years of age has been collected, we will deactivate the account and take reasonable measures to promptly delete such data from our records.If you become aware of any data we have collected from children under age 18, please contact us at bayraak@gmail.com.
            </Paragraph>


            <Heading level='3' size='m'>
                7. WHAT ARE YOUR PRIVACY RIGHTS ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short: In some regions, such as the European Economic Area, you have rights that allow you greater access to and control over your personal information.You may review, change, or terminate your account at any time.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In some regions(like the European Economic Area), you have certain rights under applicable data protection laws.These may include the right(i) to request access and obtain a copy of your personal information, (ii) to request rectification or erasure; (iii) to restrict the processing of your personal information; and(iv) if applicable, to data portability.In certain circumstances, you may also have the right to object to the processing of your personal information.To make such a request, please use the contact details provided below.We will consider and act upon any request in accordance with applicable data protection laws.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                If we are relying on your consent to process your personal information, you have the right to withdraw your consent at any time.Please note however that this will not affect the lawfulness of the processing before its withdrawal.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                If you are resident in the European Economic Area and you believe we are unlawfully processing your personal information, you also have the right to complain to your local data protection supervisory authority.You can find their contact details here: http://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.
            </Paragraph>

            <Heading level='3' size='m'>
                8. CONTROLS FOR DO - NOT - TRACK FEATURES
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Most web browsers and some mobile operating systems and mobile applications include a Do - Not - Track(“DNT”) feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected.No uniform technology standard for recognizing and implementing DNT signals has been finalized.As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online.If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this privacy notice.
            </Paragraph>

            <Heading level='3' size='m'>
                9. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short: Yes, if you are a resident of California, you are granted specific rights regarding access to your personal information.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                California Civil Code Section 1798.83, also known as the “Shine The Light” law, permits our users who are California residents to request and obtain from us, once a year and free of charge, information about categories of personal information(if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year.If you are a California resident and would like to make such a request, please submit your request in writing to us using the contact information provided below.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                If you are under 18 years of age, reside in California, and have a registered account with the Services, you have the right to request removal of unwanted data that you publicly post on the Services.To request removal of such data, please contact us using the contact information provided below, and include the email address associated with your account and a statement that you reside in California.We will make sure the data is not publicly displayed on the Services, but please be aware that the data may not be completely or comprehensively removed from our systems.
            </Paragraph>


            <Heading level='3' size='m'>
                10. DO WE MAKE UPDATES TO THIS POLICY ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                In Short: Yes, we will update this policy as necessary to stay compliant with relevant laws.
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                We may update this privacy notice from time to time.The updated version will be indicated by an updated “Revised” date and the updated version will be effective as soon as it is accessible.If we make material changes to this privacy notice, we may notify you either by prominently posting a notice of such changes or by directly sending you a notification.We encourage you to review this privacy notice frequently to be informed of how we are protecting your information.
            </Paragraph>

            <Heading level='3' size='m'>
                11. HOW CAN YOU CONTACT US ABOUT THIS POLICY ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                If you have questions or comments about this policy, you may email us at bayraak@gmail.com or by post to:
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                OnlineNotepadLite
            </Paragraph>
            <Paragraph size='small' style={{maxWidth: 550}}>
                Goce Delcev
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                43 / 8
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Gostivar, Gostivar 1230
            </Paragraph>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Macedonia
            </Paragraph>

            <Heading level='3' size='m'>
                HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE COLLECT FROM YOU ?
            </Heading>

            <Paragraph size='small' style={{maxWidth: 550}}>
                Based on the laws of some countries, you may have the right to request access to the personal information we collect from you, change that information, or delete it in some circumstances.To request to review, update, or delete your personal information, please submit a request form by clicking here.We will respond to your request within 30 days.
                This privacy policy was created using Termly’s Privacy Policy Generator.
            </Paragraph>

        </Box >
    );
}

export default PrivacyPolicy;